<?php

namespace App\Http\Controllers;

use App\Category;
use App\User;
use Illuminate\Http\Request;
use App\Tag;
use App\Role;
use App\Blog;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use function GuzzleHttp\json_decode;

class AdminController extends Controller
{   

    public function index(Request $request){
        //Birinchi Tekshiruv: Sizning logindan o`tganizni va adminmi yoki userligingizni tekshiradi
        //dd(Auth::check());
        //Auth::logout();
        if(!Auth::check() && $request->path() != 'login'){
            return redirect("/login");
        }
        if(!Auth::check() && $request->path() == 'login'){
            return view("welcome");
        }
        //dd("salom");
        $user = Auth::user();
        //dd($user->userType);
        
        if($user->role->isAdmin == 0){
            return redirect("/login");
        }
        
        if($request->path() == 'login'){
            return redirect("/");
        }

        if($request->path() == 'logout'){
            Auth::logout();
            return redirect("/login");
        }
        if($user->role->permission){
            return $this->checkForPermission($user, $request);
        }
        return view("notfound");
          
    }

    public function checkForPermission($user, $request){
        $permission = json_decode($user->role->permission);
        //dd($permission);
        $hasPermission = false;
        foreach($permission as $p){
            if($request->path() == $p->url){
                if($p->read){
                    $hasPermission = true;
                }
            }
        }
        if($hasPermission) return view("welcome");
        return view("notfound");

    }


    //Login and Logout
    public function adminLogin(Request $request){
        $this->validate($request, [
            "email" => "bail|required|email",
            "password" => "bail|required|min:3",
        ]);
        if(Auth::attempt(['email'=> $request->email, 'password'=> $request->password])){
            $user = Auth::user();
            //dd($user);
            //return $user->role->roleName;
            
            if($user->role->isAdmin == 0){
                Auth::logout();
                return response()->json([
                    'msg'=> "Incorrect login details. User not login"
                ], 401);
            }
            return response()->json([
                'msg' => "You are logged in",
                'user' => $user
            ], 200);
        }else{
            return response()->json([
                'msg'=> "Incorrect login details"
            ], 401);
        }
    }

    public function logout(){
        
        Auth::logout();
        
        return redirect("/login");
    }


    // Tag larga oid funksiyalar 
    public function addTag(Request $request){
        //validate request
        $this->validate($request, [
            "tagName" => "required|min:3"
        ]);


        return Tag::create([
            "tagName" => $request->tagName
        ]);
    }

    public function getTag(){
        //id ni kamayish tartibida oladi
        //return Tag::orderBy("id", "desc")->get();
        
        //id ni o`sish tartibida oladi
        return Tag::orderBy("id")->get();
    }

    public function editTag(Request $request){
        //validate request
        $this->validate($request, [
            "tagName" => "required",
            "id"=>"required"
        ]);

        return Tag::where("id", $request->id)->update([
            "tagName" => $request->tagName
        ]);
    }

    public function deleteTag(Request $request){
        $this->validate($request, [
            "id"=>"required"
        ]);

        return Tag::where("id", $request->id)->delete();
    }


    // Category ga oid funksiyalar
    public function upload(Request $request){
        $this->validate($request, [
            'file'=> "required|mimes:jpeg,bmp,png,jpg"
        ]);
        $picName = time().".".$request->file->extension();
        $request->file->move(public_path("uploads"), $picName);
        $picName = "/uploads/" . $picName;
        return $picName;
    }

    public function deleteImage(Request $request){
        $file = $request->iconImage;
        $this->deleteFileFromServer($file);
        return "Done!!!";
    }

    public function deleteFileFromServer($fileName){
        $filePath = public_path().$fileName;
        if(file_exists($filePath)){
            @unlink($filePath);
        }
        return "Delete SuccesFully";
    }

    public function getCategory(){
        return Category::orderBy("id")->get();
        //return $users = DB::table('categories')->get();
    }

    public function createCategory(Request $request){
        $this->validate($request, [
            "categoryName"=>"required|min:3",
            "iconImage"=>"required|min:3"
        ]);

        return Category::create([
            "categoryName"=>$request->categoryName,
            "iconImage"=>"{$request->iconImage}",
        ]);
    }

    public function deleteCategory(Request $request){
        
        $this->validate($request, [
            "id"=>"required",
            "categoryName"=>"required|min:3",
            "iconImage"=>"required|min:3"
        ]);
        
        $this->deleteImage($request);
        
        return Category::where("id", $request->id)->delete();
    }

    public function replace_image(Request $request){
        $picName = time(). "." . $request->file->extension();
        $request->file->move(public_path("/uploads/"), $picName);
        $picName = "/uploads/" . $picName;
        return $picName;
    }

    public function del_rep_img(Request $request){
        $array = $request->all();
        ;
        foreach($array as $del){
            $this->deleteFileFromServer($del);
        }
        return "SuccesFully!!!";
    }

    public function editCategory(Request $request){        
        $this->validate( $request, [
            "id"=>"required",
            "categoryName"=>"required|min:3",
            "iconImage"=>"required|min:3"
        ]); 
        return Category::where("id", $request->id)->update([
            "categoryName" => $request->categoryName,
            "iconImage" => $request->iconImage
        ]);
    }


    //AdminUsers 
    public function createUser(Request $request){
        $this->validate($request, [
            "fullName" => "required|min:3",
            "email" => "bail|required|email|unique:users",
            "password" => "bail|required|min:3",
            "role_id" => "required"
        ]);

        $password = bcrypt($request->password);
        $user = User::create([
            "fullName" => $request->fullName,
            "email" => $request->email,
            "password" => $password,
            "role_id"  => $request->role_id,
        ]);
        return $user;
       
        
    }

    public function getUsers(){
        //return User::where("userType", "!=", "User")->get();
        return User::orderBy("id")->get();
    }

    public function editUser(Request $request){
      
        $this->validate($request, [
            "fullName" => "required|min:3",
            "email" => "bail|required|email|unique:users,email,$request->id",
            "password" => "min:3",
            "role_id" => "required"
        ]);
        $data = [
            "fullName" => $request->fullName,
            "email" => $request->email,
            "role_id"  => $request->role_id,   
        ];
        if($request->password){
            $password = bcrypt($request->password);
            $data["password"] = $password;
        }
        
        $user = User::where("id", $request->id)->update($data);
        
        return $user;
    }

    public function deleteUser(Request $request){
        $this->validate($request, [
            "email" => "bail|required|email|unique:users,email,$request->id",            
        ]);
        return User::where("email", $request->email)->delete();
    }
    

    //Roles
    public function getRoles(Request $request){
        return Role::where("isAdmin", "1")->orderBy("roleName")->get();
    }

    public function createRole(Request $request){
        $this->validate($request, [
            "roleName" => "required|min:3"
        ]);

        return Role::create([
            "roleName" => $request->roleName
        ]);
    }

    public function editRole(Request $request){
        $this->validate($request, [
            "id"=>"required",
            "roleName"=>"required|min:3"
        ]);

        return Role::where("id", $request->id)->update([
            "roleName"=>$request->roleName
        ]);
    }

    public function deleteRole(Request $request){
        $this->validate($request, [
            "id"=>"required",
            "roleName"=>"required|min:3"
        ]);

        return Role::where("id", $request->id)->delete();
    }

    public function assignRole(Request $request){
        $this->validate($request, [
           "permission" => "required",
           "id"=>"required" 
        ]);

        return Role::where("id", $request->id)->update([
            "permission" => $request->permission
        ]);
    }

    //Blogga Oid
    public function createBlog(Request $request){
        $this->validate($request, [
            "title"=>"required|min:3",
            "editorData"=>"required", 
        ]);

        return Blog::create([
            "title" =>$request->title,
            "post" => "some post",
            "post_excerpt" => "read",
            
            "user_id" => Auth::id(),
            "metDescription" => "read"
        ]);
    }
}
