<?php

use App\Http\Middleware\AdminCheck;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Route::get('/{any}', function () {
//     return view('welcome');
// })->where('any','.*');

Route::prefix('app')->middleware([AdminCheck::class])->group(function(){
    Route::post('/admin_login', "AdminController@adminLogin");
    
    //Tag larga oid
    Route::post('/create_tag', "AdminController@addTag");
    Route::post('/edit_tag', "AdminController@editTag");
    Route::post('/delete_tag', "AdminController@deleteTag");
    Route::get('/get_tags', "AdminController@getTag");

    //Category ga oid
    Route::post('/upload', "AdminController@upload");
    Route::post('/delete_image', "AdminController@deleteImage");
    Route::get('/get_categories', "AdminController@getCategory");
    Route::post('/create_category', "AdminController@createCategory");
    Route::post('/delete_category', "AdminController@deleteCategory");
    Route::post('/replace_image', "AdminController@replace_image");
    Route::post('/delete_replace_images', "AdminController@del_rep_img");
    Route::post('/edit_category', "AdminController@editCategory");

    //AdminUsers ga oid
    Route::post('/create_user', "AdminController@createUser");
    Route::get('/get_users', "AdminController@getUsers");
    Route::post('/edit_user', "AdminController@editUser");
    Route::post('/delete_user', "AdminController@deleteUser");
    

    //roles
    Route::get('/get_roles', "AdminController@getRoles");
    Route::post('/create_role', "AdminController@createRole");
    Route::post('/edit_role', "AdminController@editRole");
    Route::post('/delete_role', "AdminController@deleteRole");
    Route::post('/assign_roles', "AdminController@assignRole");
    
    //Create Blogs
    Route::post("/createBlog", "AdminController@createBlog");
});





Route::get('/logout', "AdminController@logout");

Route::get('/', "AdminController@index");
Route::any('{slug}', "AdminController@index");
Route::get('/{any}',"AdminController@index")->where('any','.*');

// Route::get('/', function () {
//    return view('welcome');
//    });
    