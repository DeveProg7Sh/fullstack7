require('./bootstrap');
window.Vue = require("vue") //Vue oynasi yaratilmoqda
import router from "./router.js" //Router.js papkasida Router yozilgan, shu ulanmoqda
import store from "./store.js" //Store.js fayli ulanmoqda
import ViewUI from 'view-design';//view design ulanmoqda
import 'view-design/dist/styles/iview.css'; //view designning css linki ulanmoqda
Vue.use(ViewUI); //view design oynasidan foydalanish qo`yilmoqda
import common from "./common.js";//common.js - global ishlatilish uchun funksiyalar yozilgan.
Vue.mixin(common)// common Ishlatish uchun qo`yilmoqda

// import Editor from "vue-editor-js" //editor js ulanmoqda
// Vue.use(Editor) //Editordan foydalanish qo`yilmoqda

import CKEditor from "@ckeditor/ckeditor5-vue2"
Vue.use(CKEditor)




Vue.component("mainapp",require("./components/mainapp.vue").default);
//Vue da mainapp nomli component yaratilib , qaysi fayldan olinishi ko`rsatilmoqda

const app =new Vue({
    el: "#app", //element id berilmoqda
    router, //Router router qo`yilmoqda
    store, //Store qo`yilmoqda
    
})