import Vue from "vue"
import Router from "vue-router"
Vue.use(Router)

import firstPage from "./components/pages/myFirstPage"
import newRoutePage from "./components/pages/newRoutePage"
import hooks from "./components/pages/basic/hooks"
import methods from "./components/pages/basic/methods"
import usecom from "./vuex/usecom"

//admin projects page
import home from "./components/pages/home"
import tags from "./admin/pages/tags"
import category from "./admin/pages/category"
import createBlog from "./admin/pages/createBlog"
import adminusers from "./admin/pages/adminusers"
import login from "./admin/pages/login"
import role from "./admin/pages/role"
import assignRole from "./admin/pages/assignRole"




const routes = [
    
    //admin Projects
    {
        path: "/",
        component: home,
        name: '/'
    },
    {
        path: "/tags",
        component: tags,
        name: 'tags'
    },
    {
        path: "/category",
        component: category,
        name: 'category'
    },
    {
        path: "/createBlog",
        component: createBlog,
        name: 'createBlog'
    },
    {
        path: "/adminusers",
        component: adminusers,
        name: 'adminusers'
    },
    {
        path: "/login",
        component: login,
        name: 'login'
    },
    {
        path: "/role",
        component: role,
        name: "role"
    },
    {
        path: "/assignRole",
        component: assignRole,
        name: "assignRole"
    },
    
    
    //test vuex
    {
        path: "/testvuex",
        component: usecom
    },

    //basic tutorial router
    {
        path: "/my-router",
        component: firstPage
    },
    {
        path: "/new-route",
        component: newRoutePage
    },

    //Vue Hooks

    {
        path: "/hooks",
        component: hooks,
    },
    
    //Another Vue Hooks
    {
        path: "/methods",
        component: methods
    }

]

export default new Router({
     mode: "history",
     routes
 })
