<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="/css/all.css">
        <link rel="preconnect" href="https://fonts.gstatic.com">
        
        <title>Full Stack Blog</title>
        
        <script>
        
            (function () {
                window.Laravel = {
                    csrfToken: '{{csrf_token() }}'
                };
            })();
            
            
        </script>
    

    </head>

    
    
    
        @if(Auth::check())
        <body style = "background-color: #999 !important;">
        <div id="app" >
            <mainapp :user="{{Auth::user()}}" :permission="{{Auth::user()->role->permission}}"></mainapp>
        </div>
        @else
        <body style = "background-image: url(http://fullstack7.lo/uploads/1607336771.jpeg);  background-repeat: no-repeat">
        <div id="app">
            <mainapp :user="false"></mainapp>
        </div>
        @endif
    

    <script src="{{asset('js/app.js')}}"></script>

    </body>
</html>